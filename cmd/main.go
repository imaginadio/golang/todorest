// TodoApp REST API.
//
// written for education purposes
//
//     Schemes: http, https
//     Version: 0.1
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     SecurityDefinitions:
//     api_key:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta
package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"gitlab.com/imaginadio/golang/todorest"
	"gitlab.com/imaginadio/golang/todorest/package/handler"
	"gitlab.com/imaginadio/golang/todorest/package/repository"
	"gitlab.com/imaginadio/golang/todorest/package/service"
	"go.uber.org/zap"
)

func main() {
	logger := initLogger()
	defer logger.Sync()

	undo := zap.ReplaceGlobals(logger)
	defer undo()

	if err := initConfig(); err != nil {
		zap.L().Fatal("Error while reading config file", zap.String("msg", err.Error()))
	}

	if os.Getenv("GIN_MODE") != "release" {
		if err := godotenv.Load(); err != nil {
			zap.L().Error("Error while loading env variables", zap.String("msg", err.Error()))
		}
	}

	db, err := repository.NewPostgreDB(repository.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		Password: os.Getenv("DB_PASSWORD"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
	})

	if err != nil {
		zap.L().Fatal("Error while connecting to DB", zap.String("msg", err.Error()))
	}

	repo := repository.NewRepository(db)
	services := service.NewService(repo)
	handlers := handler.NewHandler(services)
	srv := new(todorest.Server)

	go func() {
		if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil && err != http.ErrServerClosed {
			zap.L().Fatal("Error occured while running http server", zap.String("msg", err.Error()))
		}
	}()

	zap.L().Info("TodoApp started")

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	zap.L().Info("TodoApp shutting down")

	if err := srv.Shutdown(context.Background()); err != nil {
		zap.S().Errorf("Error on server shutdown: %s", err.Error())
	}

	if err := db.Close(); err != nil {
		zap.S().Errorf("Error on db shutdown: %s", err.Error())
	}
}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}

func initLogger() *zap.Logger {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("Error while init logger: %s", err.Error())
	}
	return logger
}
