package todorest

type User struct {
	Id       string `json:"-" db:"id"`
	Name     string `json:"name" binding:"required"`
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// User info structure
// swagger:parameters signUp
type userWrapper struct {
	// in:body
	Body User
}
