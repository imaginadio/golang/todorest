package handler

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	"gitlab.com/imaginadio/golang/todorest"
	"gitlab.com/imaginadio/golang/todorest/package/service"
	mock_service "gitlab.com/imaginadio/golang/todorest/package/service/mocks"
)

func TestHandler_signUp(t *testing.T) {
	type mockBehavior func(s *mock_service.MockAuthorization, user todorest.User)

	testTable := []struct {
		name                string
		inputBody           string
		inputUser           todorest.User
		mockBehavior        mockBehavior
		expectedStatusCode  int
		expectedRequestBody string
	}{
		{
			name:      "OK",
			inputBody: `{"name":"Test","username":"test","password":"123"}`,
			inputUser: todorest.User{
				Name:     "Test",
				Username: "test",
				Password: "123",
			},
			mockBehavior: func(s *mock_service.MockAuthorization, user todorest.User) {
				s.EXPECT().CreateUser(user).Return("9379bb62-cb3d-4a8c-ae15-babfe6668ff2", nil)
			},
			expectedStatusCode:  http.StatusOK,
			expectedRequestBody: `{"result":"9379bb62-cb3d-4a8c-ae15-babfe6668ff2"}`,
		},
		{
			name:                "Empty fields",
			inputBody:           `{"username":"test","password":"123"}`,
			mockBehavior:        func(s *mock_service.MockAuthorization, user todorest.User) {},
			expectedStatusCode:  http.StatusBadRequest,
			expectedRequestBody: `{"message":"invalid input body"}`,
		},
		{
			name:      "Internal server error",
			inputBody: `{"name":"Test","username":"test","password":"123"}`,
			inputUser: todorest.User{
				Name:     "Test",
				Username: "test",
				Password: "123",
			},
			mockBehavior: func(s *mock_service.MockAuthorization, user todorest.User) {
				s.EXPECT().CreateUser(user).Return("", errors.New("service failure"))
			},
			expectedStatusCode:  http.StatusInternalServerError,
			expectedRequestBody: `{"message":"service failure"}`,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			// init dependencies
			c := gomock.NewController(t)
			defer c.Finish()

			auth := mock_service.NewMockAuthorization(c)
			tc.mockBehavior(auth, tc.inputUser)

			services := &service.Service{Authorization: auth}
			handler := NewHandler(services)

			// start test server
			r := gin.New()
			r.POST("/sign-up", handler.signUp)

			// test request
			w := httptest.NewRecorder()
			req := httptest.NewRequest(http.MethodPost, "/sign-up", bytes.NewBufferString(tc.inputBody))

			// perform request
			r.ServeHTTP(w, req)

			// assert
			assert.Equal(t, tc.expectedStatusCode, w.Code)
			assert.Equal(t, tc.expectedRequestBody, w.Body.String())
		})
	}

}
