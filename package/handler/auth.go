package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/imaginadio/golang/todorest"
)

// swagger:route POST /auth/sign-up auth signUp
//
// Register new user.
//
// Returns userId
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Schemes: http, https
//
//     Responses:
//       200: okWithResult
//       400: defaultError
//       500: defaultError
func (h *Handler) signUp(c *gin.Context) {
	var input todorest.User

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid input body")
		return
	}

	id, err := h.services.Authorization.CreateUser(input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, okResponse{Result: id})
}

type signInInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// User credentials input
// swagger:parameters signIn
type signInInputWrapper struct {
	// in:body
	Body signInInput
}

// swagger:route POST /auth/sign-in auth signIn
//
// Authorize user.
//
// Returns JWT-token
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Schemes: http, https
//
//     Responses:
//       200: okWithResult
//       400: defaultError
//       500: defaultError
func (h *Handler) signIn(c *gin.Context) {
	var input signInInput

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	token, err := h.services.Authorization.GenerateToken(input.Username, input.Password)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, okResponse{Result: token})
}
