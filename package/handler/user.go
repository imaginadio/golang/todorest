package handler

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

func getUserId(c *gin.Context) (string, error) {
	id, ok := c.Get(userCtx)
	if !ok {
		const userNotFound = "User id not found"
		newErrorResponse(c, http.StatusInternalServerError, userNotFound)
		return "", errors.New(userNotFound)
	}

	idStr, ok := id.(string)
	if !ok {
		const userInvalidType = "User id is not valid UUID"
		newErrorResponse(c, http.StatusInternalServerError, userInvalidType)
		return "", errors.New(userInvalidType)
	}

	return idStr, nil
}
