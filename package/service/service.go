package service

import (
	"gitlab.com/imaginadio/golang/todorest"
	"gitlab.com/imaginadio/golang/todorest/package/repository"
)

//go:generate mockgen -source=service.go -destination=mocks/mock.go

type Authorization interface {
	CreateUser(user todorest.User) (string, error)
	GenerateToken(username, password string) (string, error)
	ParseToken(token string) (string, error)
}

type TodoList interface {
	Create(userId string, list todorest.TodoList) (string, error)
	GetAll(userId string) ([]todorest.TodoList, error)
	GetById(userId string, listId string) (todorest.TodoList, error)
	Delete(userId, listId string) error
	Update(userId string, listId string, input todorest.UpdateListInput) error
}

type TodoItem interface {
	Create(userId string, listId string, item todorest.TodoItem) (string, error)
	GetAll(userId string, listId string) ([]todorest.TodoItem, error)
	GetById(userId string, listId string, itemId string) (todorest.TodoItem, error)
	Update(userId string, listId string, itemId string, input todorest.UpdateItemInput) error
	Delete(userId, itemId string) error
}

type Service struct {
	Authorization
	TodoList
	TodoItem
}

func NewService(repo *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(repo.Authorization),
		TodoList:      NewTodoListService(repo.TodoList),
		TodoItem:      NewTodoItemService(repo.TodoItem, repo.TodoList),
	}
}
