package service

import (
	"gitlab.com/imaginadio/golang/todorest"
	"gitlab.com/imaginadio/golang/todorest/package/repository"
)

type TodoListService struct {
	repo repository.TodoList
}

func NewTodoListService(repo repository.TodoList) *TodoListService {
	return &TodoListService{repo: repo}
}

func (s *TodoListService) Create(userId string, list todorest.TodoList) (string, error) {
	return s.repo.Create(userId, list)
}

func (s *TodoListService) GetAll(userId string) ([]todorest.TodoList, error) {
	return s.repo.GetAll(userId)
}

func (s *TodoListService) GetById(userId string, listId string) (todorest.TodoList, error) {
	return s.repo.GetById(userId, listId)
}

func (s *TodoListService) Delete(userId, listId string) error {
	return s.repo.Delete(userId, listId)
}

func (s *TodoListService) Update(userId string, listId string, input todorest.UpdateListInput) error {
	if err := todorest.ValidateInputStruct(input); err != nil {
		return err
	}
	return s.repo.Update(userId, listId, input)
}
