package service

import (
	"gitlab.com/imaginadio/golang/todorest"
	"gitlab.com/imaginadio/golang/todorest/package/repository"
)

type TodoItemService struct {
	repo     repository.TodoItem
	repoList repository.TodoList
}

func NewTodoItemService(repo repository.TodoItem, repoList repository.TodoList) *TodoItemService {
	return &TodoItemService{
		repo:     repo,
		repoList: repoList,
	}
}

func (s *TodoItemService) Create(userId string, listId string, item todorest.TodoItem) (string, error) {
	if _, err := s.repoList.GetById(userId, listId); err != nil {
		return "", err
	}

	return s.repo.Create(listId, item)
}

func (s *TodoItemService) GetAll(userId string, listId string) ([]todorest.TodoItem, error) {
	return s.repo.GetAll(userId, listId)
}

func (s *TodoItemService) GetById(userId string, listId string, itemId string) (todorest.TodoItem, error) {
	return s.repo.GetById(userId, listId, itemId)
}

func (s *TodoItemService) Update(userId string, listId string, itemId string, input todorest.UpdateItemInput) error {
	if err := todorest.ValidateInputStruct(input); err != nil {
		return err
	}
	return s.repo.Update(userId, listId, itemId, input)
}

func (s *TodoItemService) Delete(userId, itemId string) error {
	return s.repo.Delete(userId, itemId)
}
