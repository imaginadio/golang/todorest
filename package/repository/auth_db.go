package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/imaginadio/golang/todorest"
)

type AuthDB struct {
	db *sqlx.DB
}

func NewAuth(db *sqlx.DB) *AuthDB {
	return &AuthDB{db: db}
}

func (r *AuthDB) CreateUser(user todorest.User) (string, error) {
	var id string
	query := fmt.Sprintf("INSERT INTO %s (name, username, password_hash) values ($1, $2, $3) RETURNING id", tusers)

	row := r.db.QueryRow(query, user.Name, user.Username, user.Password)
	if err := row.Scan(&id); err != nil {
		return "", err
	}

	return id, nil
}

func (r *AuthDB) GetUser(username, password string) (todorest.User, error) {
	var user todorest.User
	query := fmt.Sprintf("SELECT id FROM %s WHERE username = $1 and password_hash = $2", tusers)
	err := r.db.Get(&user, query, username, password)

	return user, err
}
