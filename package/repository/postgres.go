package repository

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const (
	tusers      = "users"
	ttodoLists  = "todo_lists"
	tusersLists = "users_lists"
	ttodoItems  = "todo_items"
	tlistsItems = "lists_items"
)

type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
	SSLMode  string
}

func NewPostgreDB(cfg Config) (*sqlx.DB, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		cfg.Host, cfg.Port, cfg.Username, cfg.DBName, cfg.Password, cfg.SSLMode))
	if err != nil {
		return nil, err
	}

	return db, nil
}

func prepareUpdate(s interface{}) (string, []interface{}) {
	updateStrings := make([]string, 0)
	updateValues := make([]interface{}, 0)

	reflectValues := reflect.ValueOf(s)
	reflectTypes := reflectValues.Type()

	for i := 0; i < reflectValues.NumField(); i++ {
		tag, ok := reflectTypes.Field(i).Tag.Lookup("db")
		if ok && !reflectValues.Field(i).IsNil() {
			updateStrings = append(updateStrings, fmt.Sprintf("%s=$%d", tag, len(updateStrings)+1))
			updateValues = append(updateValues, reflectValues.Field(i).Interface())
		}
	}

	return strings.Join(updateStrings, ", "), updateValues
}
