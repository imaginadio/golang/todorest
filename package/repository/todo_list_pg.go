package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/imaginadio/golang/todorest"
	"go.uber.org/zap"
)

type TodoListPg struct {
	db *sqlx.DB
}

func NewTodoListPg(db *sqlx.DB) *TodoListPg {
	return &TodoListPg{db: db}
}

func (r *TodoListPg) Create(userId string, list todorest.TodoList) (string, error) {
	tr, err := r.db.Begin()
	if err != nil {
		return "", err
	}

	var id string
	queryCreateList := fmt.Sprintf("INSERT INTO %s (title, description) VALUES ($1, $2) RETURNING id", ttodoLists)
	row := r.db.QueryRow(queryCreateList, list.Title, list.Description)
	if err := row.Scan(&id); err != nil {
		tr.Rollback()
		return "", err
	}

	queryCreateUserList := fmt.Sprintf("INSERT INTO %s (user_id, list_id) VALUES ($1, $2)", tusersLists)
	if _, err := r.db.Exec(queryCreateUserList, userId, id); err != nil {
		tr.Rollback()
		return "", err
	}

	return id, tr.Commit()
}

func (r *TodoListPg) GetAll(userId string) ([]todorest.TodoList, error) {
	var todoList []todorest.TodoList

	query := fmt.Sprintf("SELECT tl.id, tl.title, tl.description FROM %s ul INNER JOIN %s tl ON ul.list_id = tl.id WHERE ul.user_id = $1", tusersLists, ttodoLists)
	err := r.db.Select(&todoList, query, userId)

	return todoList, err
}

func (r *TodoListPg) GetById(userId string, listId string) (todorest.TodoList, error) {
	var todoList todorest.TodoList

	query := fmt.Sprintf(`SELECT tl.id, tl.title, tl.description FROM %s tl INNER JOIN %s ul ON tl.id = ul.list_id 
	                       WHERE ul.user_id = $1 AND tl.id = $2`, ttodoLists, tusersLists)
	err := r.db.Get(&todoList, query, userId, listId)

	return todoList, err
}

func (r *TodoListPg) Delete(userId, listId string) error {
	query := fmt.Sprintf("DELETE FROM %s tl USING %s ul WHERE tl.id = ul.list_id AND ul.user_id=$1 AND ul.list_id=$2",
		ttodoLists, tusersLists)
	_, err := r.db.Exec(query, userId, listId)

	return err
}

func (r *TodoListPg) Update(userId string, listId string, input todorest.UpdateListInput) error {
	updateStr, attrs := prepareUpdate(input)
	query := fmt.Sprintf("UPDATE %s tl SET %s FROM %s ul WHERE tl.id = ul.list_id AND ul.user_id = $%d AND ul.list_id = $%d", ttodoLists, updateStr, tusersLists, len(attrs)+1, len(attrs)+2)
	attrs = append(attrs, userId, listId)

	zap.S().Debugf("query string: %s", query)
	zap.S().Debugf("query arguments: %s", attrs)

	_, err := r.db.Exec(query, attrs...)
	return err
}
