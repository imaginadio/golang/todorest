package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/imaginadio/golang/todorest"
)

type Authorization interface {
	CreateUser(user todorest.User) (string, error)
	GetUser(username, password string) (todorest.User, error)
}

type TodoList interface {
	Create(userId string, list todorest.TodoList) (string, error)
	GetAll(userId string) ([]todorest.TodoList, error)
	GetById(userId string, listId string) (todorest.TodoList, error)
	Delete(userId, listId string) error
	Update(userId string, listId string, input todorest.UpdateListInput) error
}

type TodoItem interface {
	Create(listId string, item todorest.TodoItem) (string, error)
	GetAll(userId string, listId string) ([]todorest.TodoItem, error)
	GetById(userId string, listId string, itemId string) (todorest.TodoItem, error)
	Update(userId string, listId string, itemId string, input todorest.UpdateItemInput) error
	Delete(userId, itemId string) error
}

type Repository struct {
	Authorization
	TodoList
	TodoItem
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Authorization: NewAuth(db),
		TodoList:      NewTodoListPg(db),
		TodoItem:      NewTodoItemPg(db),
	}
}
