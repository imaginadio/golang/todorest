package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/imaginadio/golang/todorest"
	"go.uber.org/zap"
)

type TodoItemPg struct {
	db *sqlx.DB
}

func NewTodoItemPg(db *sqlx.DB) *TodoItemPg {
	return &TodoItemPg{db: db}
}

func (r *TodoItemPg) Create(listId string, item todorest.TodoItem) (string, error) {
	tr, err := r.db.Begin()
	if err != nil {
		return "", err
	}

	var itemId string
	query := fmt.Sprintf("INSERT INTO %s (title, description) VALUES ($1, $2) RETURNING id", ttodoItems)
	row := r.db.QueryRow(query, item.Title, item.Description)
	if err := row.Scan(&itemId); err != nil {
		tr.Rollback()
		return "", err
	}

	query = fmt.Sprintf("INSERT INTO %s (list_id, item_id) VALUES ($1, $2)", tlistsItems)
	if _, err := r.db.Exec(query, listId, itemId); err != nil {
		tr.Rollback()
		return "", err
	}

	return itemId, tr.Commit()
}

func (r *TodoItemPg) GetAll(userId string, listId string) ([]todorest.TodoItem, error) {
	var todoItems []todorest.TodoItem

	query := fmt.Sprintf(`SELECT ti.id, ti.title, ti.description, ti.done
												  FROM %s ti
												 INNER JOIN %s tli ON ti.id = tli.item_id
												 INNER JOIN %s ul ON tli.list_id = ul.list_id
												 WHERE ul.user_id = $1
												   AND ul.list_id = $2`, ttodoItems, tlistsItems, tusersLists)
	err := r.db.Select(&todoItems, query, userId, listId)

	return todoItems, err
}

func (r *TodoItemPg) GetById(userId string, listId string, itemId string) (todorest.TodoItem, error) {
	var todoItem todorest.TodoItem

	query := fmt.Sprintf(`SELECT ti.id, ti.title, ti.description, ti.done
												  FROM %s ti
												 INNER JOIN %s tli ON ti.id = tli.item_id
												 INNER JOIN %s ul ON tli.list_id = ul.list_id
												 WHERE ul.user_id = $1
												   AND ul.list_id = $2
													 AND ti.id = $3`, ttodoItems, tlistsItems, tusersLists)
	err := r.db.Get(&todoItem, query, userId, listId, itemId)

	return todoItem, err
}

func (r *TodoItemPg) Update(userId string, listId string, itemId string, input todorest.UpdateItemInput) error {
	updateStr, attrs := prepareUpdate(input)
	query := fmt.Sprintf(`UPDATE %s ti
												   SET %s
													FROM %s ul
												 INNER JOIN %s tli ON ul.list_id = tli.list_id
												 WHERE ti.id = tli.item_id
												   AND ul.user_id = $%d
													 AND ul.list_id = $%d
													 AND ti.id = $%d`, ttodoItems, updateStr, tusersLists, tlistsItems, len(attrs)+1, len(attrs)+2, len(attrs)+3)
	attrs = append(attrs, userId, listId, itemId)

	zap.S().Debugf("query string: %s", query)
	zap.S().Debugf("query arguments: %s", attrs)

	_, err := r.db.Exec(query, attrs...)
	return err
}

func (r *TodoItemPg) Delete(userId, itemId string) error {
	query := fmt.Sprintf(`DELETE FROM %s ti USING %s li, %s ul 
									WHERE ti.id = li.item_id AND li.list_id = ul.list_id AND ul.user_id = $1 AND ti.id = $2`,
		ttodoItems, tlistsItems, tusersLists)
	_, err := r.db.Exec(query, userId, itemId)
	return err
}
