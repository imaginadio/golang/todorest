package repository

import (
	"errors"
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/imaginadio/golang/todorest"
	"go.uber.org/zap"
)

func TestTodoItemPg_Create(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		zap.S().Fatalf("error while create mock db: %s", err.Error())
	}

	defer mockDB.Close()

	db := sqlx.NewDb(mockDB, "sqlmock")

	r := NewTodoItemPg(db)

	type args struct {
		listId string
		item   todorest.TodoItem
	}
	type mockBehaviour func(args args, id string)

	testTable := []struct {
		name          string
		mockBehaviour mockBehaviour
		args          args
		id            string
		expectError   bool
	}{
		{
			name: "OK",
			args: args{
				listId: "d69ea758-57df-4939-b2a3-11383ba94ab1",
				item: todorest.TodoItem{
					Title:       "test title",
					Description: "test description",
				},
			},
			id: "059990e1-272d-4f4a-9d46-e2b467b21bc5",
			mockBehaviour: func(args args, id string) {
				mock.ExpectBegin()

				rows := sqlmock.NewRows([]string{"id"}).AddRow(id)
				mock.ExpectQuery(fmt.Sprintf("INSERT INTO %s", ttodoItems)).WithArgs(args.item.Title, args.item.Description).WillReturnRows(rows)
				mock.ExpectExec(fmt.Sprintf("INSERT INTO %s", tlistsItems)).WithArgs(args.listId, id).WillReturnResult(sqlmock.NewResult(1, 1))

				mock.ExpectCommit()
			},
		},
		{
			name: "Empty fields",
			args: args{
				listId: "d69ea758-57df-4939-b2a3-11383ba94ab1",
				item: todorest.TodoItem{
					Title:       "",
					Description: "test description",
				},
			},
			mockBehaviour: func(args args, id string) {
				mock.ExpectBegin()

				rows := sqlmock.NewRows([]string{"id"}).AddRow(id).RowError(1, errors.New("empty fields"))
				mock.ExpectQuery(fmt.Sprintf("INSERT INTO %s", ttodoItems)).WithArgs(args.item.Title, args.item.Description).WillReturnRows(rows)

				mock.ExpectRollback()
			},
			expectError: true,
		},
		{
			name: "Error on 2nd insert",
			args: args{
				listId: "d69ea758-57df-4939-b2a3-11383ba94ab1",
				item: todorest.TodoItem{
					Title:       "test title",
					Description: "test description",
				},
			},
			id: "059990e1-272d-4f4a-9d46-e2b467b21bc5",
			mockBehaviour: func(args args, id string) {
				mock.ExpectBegin()

				rows := sqlmock.NewRows([]string{"id"}).AddRow(id)
				mock.ExpectQuery(fmt.Sprintf("INSERT INTO %s", ttodoItems)).WithArgs(args.item.Title, args.item.Description).WillReturnRows(rows)
				mock.ExpectExec(fmt.Sprintf("INSERT INTO %s", tlistsItems)).WithArgs(args.listId, id).WillReturnError(errors.New("insert error"))

				mock.ExpectRollback()
			},
			expectError: true,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockBehaviour(tc.args, tc.id)

			got, err := r.Create(tc.args.listId, tc.args.item)

			if tc.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tc.id, got)
			}

			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}
