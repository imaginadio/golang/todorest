package todorest

import (
	"errors"
	"reflect"
)

type TodoList struct {
	Id          string `json:"id" db:"id"`
	Title       string `json:"title" db:"title" binding:"required"`
	Description string `json:"description" db:"description"`
}

// TodoList fields
// swagger:parameters createList
type todoListWrapper struct {
	// in:body
	Body TodoList
}

type UpdateListInput struct {
	Title       *string `json:"title" db:"title"`
	Description *string `json:"description" db:"description"`
}

type UpdateItemInput struct {
	Title       *string `json:"title" db:"title"`
	Description *string `json:"description" db:"description"`
	Done        *bool   `json:"done" db:"done"`
}

func ValidateInputStruct(s interface{}) error {
	reflectValues := reflect.ValueOf(s)

	for i := 0; i < reflectValues.NumField(); i++ {
		if !reflectValues.Field(i).IsNil() {
			return nil
		}
	}

	return errors.New("Structure has no values")
}

type UsersList struct {
	Id     int
	UserId string
	ListId string
}

type TodoItem struct {
	Id          string `json:"id" db:"id"`
	Title       string `json:"title" db:"title"`
	Description string `json:"description" db:"description"`
	Done        bool   `json:"done" db:"done"`
}

type ListItem struct {
	Id     int
	ListId string `db:"list_id"`
	ItemId string `db:"item_id"`
}
