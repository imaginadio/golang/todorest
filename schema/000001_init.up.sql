CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users
(
    id uuid primary key default uuid_generate_v4(),
    name varchar(255),
    username varchar(255) not null unique,
    password_hash varchar(255) not null
);

CREATE TABLE todo_lists
(
    id uuid primary key default uuid_generate_v4(),
    title varchar(255) not null,
    description varchar(4000)
);

CREATE TABLE users_lists
(
    id serial not null unique,
    user_id uuid references users on delete cascade,
    list_id uuid references todo_lists on delete cascade
);

CREATE TABLE todo_items
(
    id uuid primary key default uuid_generate_v4(),
    title varchar(255) not null,
    description varchar(4000),
    done boolean not null default false
);

CREATE TABLE lists_items
(
    id serial not null unique,
    list_id uuid references todo_lists on delete cascade,
    item_id uuid references todo_items on delete cascade
);